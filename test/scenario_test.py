__author__ = 'mlei'

import logging
from src.attacker import *
from src.victim import *
from src.utils import *
from src.scenario_1 import *
from src.config import *

#tx_delays = [0.1, 0.3, 0.5, 0.7, 0.9]
#tx_delays = [0.0]
#tx_delays = [1.1, 1.0, 1.1, 1.0]
tx_delays = [1.0, 0.8, 0.9]

def start():
    scenarios = []
    for delay in tx_delays:
        for i in range(0, N_SCENARIOS):
            s = scenario_1(delay)
            del s

if __name__ == "__main__":
    start()