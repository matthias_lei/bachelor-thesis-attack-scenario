__author__ = 'mlei'

import logging
from src.config import *
from src.scenario_1 import scenario_1
from src.utils import NLock
from StringIO import StringIO
from bitcoin.messages import *

MAGIC = 'D9B4BEF9'.decode("hex")[::-1]

def read(length, buffer):
    """Read exactly read_len bytes or raise ValueError.

    gevent does not like allocating arbitrary size buffers. We need to
    reassemble it here, otherwise large packets kill the connection.
    """
    payload = BytesIO()
    read_len = 0
    while read_len < length:
        b = buffer.read(length - read_len)
        l = len(b)
        if not l:
            raise ValueError('Short read from socket (%d != %d)'
                             % (length, len(b)))
        read_len += l
        payload.write(b)
    payload.seek(0)
    return payload


def read_message(buf):
    header = read(24, buf)

    magic, command, length, unused_checksum = struct.unpack(
        "<4s12sII", header.getvalue())
    print str(magic.encode('hex')) + " expected: " + str(MAGIC.encode('hex'))
    if MAGIC != magic:
        raise ValueError('Message separator magic did not match protocol '
                         '(%s)' % magic.encode('hex'))
    payload = read(length, buf)

    command = command.strip('\x00')
    inv = InvPacket()
    inv.parse(payload, 70001)
    return inv

def start():
    #s = "f9beb4d9696e7600000000000000000025000000".decode('hex')
    s = "f9beb4d9696e76000000000000000000250000007f0686d501010000006be8b4bd17937f8f75a18b5cfa3fb31405058884209863dda073f43279a92b36".decode('hex')
    buffer = StringIO()
    buffer.write(s)
    buffer.seek(0)
    buffer.flush()
    inv = read_message(buffer)
    pass



if __name__ == "__main__":
    start()