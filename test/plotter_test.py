__author__ = 'mlei'


import logging
from src.plotter import *
from src.evaluator import *
from src.meta_file import *
from src.config import *
from os.path import join

logging.getLogger().setLevel(logging.DEBUG)


FILE_HEADER = "1433287516.89"
PATH = join(SCENARIOS_FOLDER_NAME, SCENARIO_PREFIX + FILE_HEADER)

meta_data = load_meta_file(join(PATH, META_DATA_NAME + META_DATA_EXT))

print str(meta_data)
plot = plotter(float(FILE_HEADER),
                    PATH,
                    meta_data['tx_a'],
                    meta_data['tx_v'],
                    meta_data['inv_a'],
                    meta_data['inv_v'],
                    meta_data['tx_v_send_time'],
                    meta_data['tx_a_send_time'],
                    meta_data['inv_send_time'])

eval = evaluator(float(FILE_HEADER),
                 PATH,
                 meta_data['tx_a'],
                 meta_data['tx_v'],
                 meta_data['inv_a'],
                 meta_data['inv_v'])