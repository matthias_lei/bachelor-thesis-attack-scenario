from src.btc_client import tx_in_blockchain, restart_client, client_is_running

txid1 = '0dfbbf13fb22be1a9d99e520c6937b7a63253b99967032eb9100e2c2e0e1ac6d'
txid2 = '553a045bb69d4b75e86a743076ca293220e100aee9d4866f4e7c07939e9ad4fb'

if not client_is_running():
    restart_client()

print "tx1 in chain: " + str(tx_in_blockchain(txid1))
print "tx2 in chain: " + str(tx_in_blockchain(txid2))