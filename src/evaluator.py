from src.config import RESULT_FILE_NAME

__author__ = 'mlei'

import logging
from config import *
from os.path import join
from node_logger import load_all_slog_files
from meta_file import *
from utils import *
from json import dumps
from copy import deepcopy


class evaluator(object):

    def __init__(self, timestamp, path, tx_a, tx_v, inv_a, inv_v, ):
        self.timestamp = timestamp
        self.path = path

        self.tx_a = tx_a
        self.tx_v = tx_v
        self.inv_a = inv_a
        self.inv_v = inv_v

        self.victim_true_peers = load_victim_meta_file(join(self.path, VICTIM_DATA_FILE_NAME + META_DATA_EXT))
        self.attacker_local_peers, self.attacker_other_peers = load_attacker_meta(join(self.path, ATTACKER_META_FILE_NAME + META_DATA_EXT))

        self.victim_data = load_all_slog_files(self.path)
        self.attacker_data = load_attacker_log(join(self.path, str(timestamp) + A_LOG_FILE_EXT))

        self.result_file = open(join(self.path, RESULT_FILE_NAME + META_DATA_EXT), 'w')

        self.write_tx_victim_evaluation()

        self.result_file.close()

    def _filter_txs(self):
        tx_as = [d for d in self.victim_data if d['type'] == 'tx' and tx_equals(d['packet'], self.tx_a)]
        tx_vs = [d for d in self.victim_data if d['type'] == 'tx' and tx_equals(d['packet'], self.tx_v)]

        tx_as = sorted(tx_as, lambda x, y: cmp(x['timestamp'], y['timestamp']))
        tx_vs = sorted(tx_vs, lambda x, y: cmp(x['timestamp'], y['timestamp']))

        return tx_as, tx_vs
        # earliest_tx_a = min(tx_as, key=(lambda x, y: cmp(x['timestamp'], y['timestamp']))) if not len(tx_as) == 0 else None
        # earliest_tx_v = min(tx_vs, key=(lambda x, y: cmp(x['timestamp'], y['timestamp']))) if not len(tx_vs) == 0 else None

    def write_tx_victim_evaluation(self):
        tx_as, tx_vs = deepcopy(self._filter_txs())

        #dont dump packet
        [d.pop('packet', None) for d in tx_as]
        [d.pop('packet', None) for d in tx_vs]

        self.result_file.write("Tx_v received\n")
        self.result_file.write(dumps(tx_vs) + "\n")

        self.result_file.write("Tx_a received\n")
        self.result_file.write(dumps(tx_as) + "\n")

        if (not len(tx_as) == 0) and (not len(tx_vs) == 0):
            time_tx_a = float(min(tx_as, key=lambda x: float(x['timestamp']))['timestamp'])
            time_tx_v = float(min(tx_vs, key=lambda x: float(x['timestamp']))['timestamp'])
            self.result_file.write("Transaction timeframe: " + str(time_tx_a - time_tx_v))

    def write_attacker_connection_evaluation(self):
        other_peers = self.attacker.other_peer_connections
        local_peers = self.attacker.local_peer_connections

        self.result_file.write("Local peer connections: " + str(len(local_peers)) + " out of " + str(N_OTHER_PEERS))
        self.result_file.write("Other peer connections: " + str(len(other_peers)) + " out of " + str(N_TRUE_LOCAL_PEERS + N_FALSE_POSITIVE_PEERS))

