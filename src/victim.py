from bitcoin.network import ConnectionEstablishedEvent

__author__ = 'mlei'

import logging
from node_logger import *
from utils import *
from bitcoin import network, messages
from gevent import sleep, spawn
from threading import Thread
from meta_file import write_victim_meta_file
from utils import NLock
from gevent.lock import Semaphore


class victim(object):

    def __init__(self, path, timestamp, true_peers_pool, attacker):
        self.path = path
        self.timestamp = timestamp
        self.true_peers_pool = true_peers_pool
        self.peer_loggers = {}
        self.true_peer_connections = []
        self.attacker = attacker
        self.connection_lock = Semaphore(1)
        self.active_connection_counter = 0

        for peer in self.true_peers_pool:
            host = peer[0]
            port = peer[1]
            n_logger = node_logger(self.timestamp, self.path, host, port)
            n_logger.register_handler(ConnectionEstablishedEvent.type, self._on_connect_signal)
            n_logger.register_handler('inv', self._on_inv_signal)
            n_logger.register_handler('tx', self._on_tx_signal)
            self.peer_loggers[(host, port)] = n_logger

        #TODO why did i connected to attacker?
        # self.attacker_logger = node_logger(self.timestamp, self.attacker[0], self.attacker[1])
        # self.attacker_logger.register_handler(ConnectionEstablishedEvent.type, self._on_connect_signal)
        #self.attacker_logger.register_handler('inv', self._on_inv_signal)

        self._lock = None

    def connect_to_all_peers(self):
        self._lock = NLock(len(self.true_peers_pool) + 1, timeout=30)
        #TODO expand peerlogger list & NLock doesnt takes N_TRUE_LOCAL_PEERS as parameter
        for logger in self.peer_loggers.values():
            spawn(logger.start)
        # spawn(self.attacker_logger.start)

        logging.debug("waiting for completion")

        self._lock.acquire()

        #cleanup idle node_loggers
        for n_logger in self.peer_loggers.values():
            host = n_logger.host
            port = n_logger.port
            if not (host, port) in self.true_peer_connections:
                del self.peer_loggers[(host, port)]

    def _on_connect_signal(self, connection, unused_message):
        logging.info("victim connected to " + str(connection.host))

        self.connection_lock.acquire()
        if self.active_connection_counter < N_TRUE_LOCAL_PEERS:
            self.peer_loggers[connection.host].start_logging()
            self.true_peer_connections.append(connection.host)
            self.active_connection_counter += 1
        self.connection_lock.release()

        self._lock.release()

    def stop(self):
        [l.stop() for l in self.peer_loggers.values()]
        write_victim_meta_file(self.path, self)

    def _on_inv_signal(self, connection, inv):
        logging.info("victim received inv message from " + str(connection.host))
        getdata = GetDataPacket()
        getdata.hashes = inv.hashes
        connection.send('getdata', getdata)

    def _on_tx_signal(self, connection, tx):
        logging.info("victim received tx message from " + str(connection.host))