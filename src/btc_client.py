from bitcoin.messages import TxPacket, InvPacket

__author__ = 'mlei'

import logging
from subprocess import Popen, call, check_output, PIPE
import time
from config import *
import json
from bitcoinrpc.authproxy import AuthServiceProxy
from bitcoin import messages
from StringIO import StringIO


def create_inv_from_tx(tx):
    inv = InvPacket()
    type = 1
    inv.hashes = [(type, tx.hash())]
    logging.debug("inv packet created: " + str(tx.hash()))
    return inv


def close_client():
    Popen(["pkill", "bitcoin-qt"])
    time.sleep(3)


def restart_client():
    Popen(["pkill", "bitcoin-qt"])
    time.sleep(3)
    cmd = ["bitcoin-qt", "-server"]
    if TEST_MODE:
        cmd.append("-testnet")
    #cmd.append("&")
    Popen(cmd)
    time.sleep(1)


def client_is_running():
    cmd = 'pgrep'
    args = 'bitcoin-qt'
    # script = check_output([cmd, args])
    # print str(script)
    out = Popen([cmd, args], stdout=PIPE).stdout
    for line in out:
        logging.debug("pgrep result: " + str(line))
        if line.strip("\n").isdigit():
            logging.debug("client_is_running: True")
            return True
    logging.debug("client_is_running: False")
    return False


def get_tx_conf(txid):
    response = _issue_query(["gettransaction", txid])
    try:
        #print str(response)
        return json.loads(response)
    except ValueError:
        return None

def tx_in_blockchain(txid):
    tx_info = get_tx_conf(txid)
    return tx_info is not None and 'confirmations' in tx_info and tx_info['confirmations'] > 0


def get_unspent_output():
    """
    :return: json object
    """
    return json.loads(_issue_query(["listunspent", "0"]))

def get_new_address():
    return _issue_query(["getnewaddress"])

def create_tx_packet(input_element, output_address, amount, change_address=None):
    """
    creates signed tx packet
    """
    raw_tx = create_signed_tx_hex(input_element, output_address, amount, change_address)

    if not raw_tx['complete']:
        raise ValueError("transaction not complete")

    raw_tx_buf = StringIO()
    raw_tx_buf.write(raw_tx['hex'].decode('hex'))
    raw_tx_buf.flush()
    raw_tx_buf.seek(0)

    #logging.debug("raw_tx as hex encoded: " + str(raw_tx_buf.readline(100000000000)))

    tx_packet = TxPacket()
    tx_packet.parse(raw_tx_buf, messages.PROTOCOL_VERSION)
    return tx_packet


def create_raw_tx_hex(input_element, output, amount, change_address=None):
    """
    assumes the vout 0 is source input (and not change, or whatever)
    NOTE: createrawtransaction's input is hex encoded, but the output parameter is a string!!!!
    :param input: txid (as string, not as hex)
    :param output: hashaddress
    :param amount:
    :return:
    """
    cmd = ["createrawtransaction"]
    input_param = "[{\"txid\":\"%s\",\"vout\": %d}]" % (str(input_element['txid']), input_element['vout'])
    output_param = "{\"%s\":%f" % (str(output), amount-TRANSACTION_FEE)

    if change_address:
        output_param = output_param + " ,\"%s\":%f}" % (str(change_address), input_element['amount']-amount)
    else:
        output_param = output_param + "}"

    cmd.append(input_param)
    cmd.append(output_param)

    logging.debug("create_raw_tx: " + str(cmd))

    result = _issue_query(cmd)

    logging.debug("create_raw_tx result: " + str(result))

    return result

def decode_raw_tx_hex(hex):
    cmd = ["decoderawtransaction"]
    cmd.append(str(hex).strip("\n"))

    result = json.loads(_issue_query(cmd))
    return result

def create_signed_tx_hex(input_element, output, amount, change_address=None):
    if input_element['amount'] < amount:
        raise ValueError("insufficent balance (input amount: " + str(input_element['amount']) + " output amount: " + str(amount))
    raw_hex_tx = create_raw_tx_hex(input_element, output, amount, change_address)
    cmd = ["signrawtransaction"]
    cmd.append(str(raw_hex_tx))

    logging.debug("sign_raw_tx: " + str(cmd))

    result = json.loads(_issue_query(cmd))

    logging.debug("sign_raw_tx result: " + str(result))

    return result


def create_inv_message():
    pass


def _issue_command(cmd):
    rpc = ["bitcoin-cli"]
    if TEST_MODE:
        rpc.append("-testnet")
    rpc.append("-rpcwait")
    rpc = rpc + cmd
    logging.debug("issue_commad: " + str(rpc))
    if client_is_running():
        call(rpc)
    else:
        raise ClientNotRunning()


def _issue_query(cmd):
    """
    :param cmd: list of strings [<rpc>, <params>...]
    :return:
    """
    result = ""
    rpc = ["bitcoin-cli"]
    if TEST_MODE:
        rpc.append("-testnet")
    rpc.append("-rpcwait")
    rpc = rpc + cmd

    logging.debug("issue_query: " + str(rpc))

    if client_is_running():
        query = Popen(rpc, stdout=PIPE)
    else:
        raise ClientNotRunning()

    for line in query.stdout:
        result = result + line

    logging.debug("issue_query result: " + str(result))

    return result.strip("\n")

def merge_outputs():
    unspent_outs = get_unspent_output()

    destination = get_new_address()
    cmd = ["createrawtransaction"]
    inputs = []
    outputs = {}
    amount = 0

    for u in unspent_outs:
        inputs.append({'txid': u['txid'], 'vout': u['vout']})
        amount += u['amount']

    outputs[destination] = amount - TRANSACTION_FEE

    cmd.append(json.dumps(inputs))
    cmd.append(json.dumps(outputs))
    print "cmd: " + str(cmd)

    raw_tx = _issue_query(cmd)
    print "raw_tx: " + str(raw_tx)

    signed_tx = json.loads(_issue_query(["signrawtransaction", raw_tx]))
    print "signed_tx: " + str(signed_tx)

    _issue_command(["sendrawtransaction", signed_tx['hex']])


class ClientNotRunning(Exception):
    def __str__(self):
        return "Client not running"
