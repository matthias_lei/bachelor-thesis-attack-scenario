from twisted.trial.test import packages

__author__ = 'mlei'

#from distutils.core import setup
from setuptools import setup

setup(name='attacke_scenario',
      version='1.0',
      py_modules=['attacker',
                  'btc_client',
                  'config',
                  'evaluator',
                  'meta_file',
                  'node_logger',
                  'plotter',
                  'scenario_1',
                  'utils',
                  'victim'])

# setup(name='attacke_scenario',
#       version='1.0',
#       packages=['src'])


# setup(name='attacke_scenario',
#       version='1.0',
#       py_modules=['src'])