from copy_reg import pickle

__author__ = 'mlei'

from bitcoin import network, common
from bitcoin.messages import *
from os import listdir
import os
from config import *
from os.path import join, exists, isfile, isdir
import time
import logging
from shutil import rmtree
from pickle import dumps, loads
import re



class node_logger(object):
    """
    only creates a file if connection has been established
    """

    def __init__(self, timestamp, path, host, port):

        self.timestamp = timestamp
        self.host = host
        self.port = port
        #self.path = join(SCENARIOS_FOLDER_NAME, SCENARIO_PREFIX + str(timestamp))
        self.path = path
        self.file_name = str(self.timestamp) + "_" + str(self.host) + ":" + str(self.port)
        self.log_file = None
        self.client = network.GeventNetworkClient()

        self._register_handlers()

    def start(self):
        self.client.connect((self.host, self.port))
        self.client.run_forever()

    def stop(self):
        try:
            self.client.disconnect((self.host, self.port))
        except (ValueError, KeyError):
            pass
        self.log_file.close()

    def start_logging(self):
        if not exists(SCENARIOS_FOLDER_NAME):
            os.mkdir(SCENARIOS_FOLDER_NAME)
        if not exists(self.path):
            os.mkdir(self.path)
        if not exists(join(self.path, self.file_name + V_LOG_FILE_EXT)):
            self.log_file = open(join(self.path, self.file_name + V_LOG_FILE_EXT), 'w')
        # else:
        #     rmtree(self.path)
        #     os.mkdir(self.path)

    def register_handler(self, msg_type, handler):
        self.client.register_handler(msg_type, handler)

    def _register_handlers(self):
        self.client.register_handler('version', self._on_version_received)
        self.client.register_handler('inv', self._on_inv_received)
        self.client.register_handler('getdata', self._on_getdata_received)
        self.client.register_handler('ping', self._on_ping_received)
        self.client.register_handler('tx', self._on_tx_received)
        self.client.register_handler('block', self._on_block_received)
        self.client.register_handler('addr', self._on_addr_received)
        self.client.register_handler('verack', self._on_verack_received)
        self.client.register_handler('getaddr', self._on_verack_received)
        network.ClientBehavior(self.client)

    def _on_version_received(self, connection, version):
        # timestamp = str(time.time())
        logging.debug("version packet received from " + str(self.host) + ":" + str(self.port))
        #
        # self._dump_msg('version', timestamp, version)


    def _on_inv_received(self, connection, inv):
        timestamp = str(time.time())
        logging.debug("inv packet received from " + str(self.host) + ":" + str(self.port))

        self._dump_msg('inv', timestamp, inv)


    def _on_getdata_received(self, connection, getdata):
        timestamp = str(time.time())
        logging.debug("getdata packet received from " + str(self.host) + ":" + str(self.port))

        self._dump_msg('getdata', timestamp, getdata)


    def _on_ping_received(self, connection, ping):
        timestamp = str(time.time())
        logging.debug("ping packet received from " + str(self.host) + ":" + str(self.port))
        #TODO ping has problems dumping the nonce field; approach special handling
        self._dump_msg('ping', timestamp, ping)


    def _on_tx_received(self, connection, tx):
        timestamp = str(time.time())
        logging.debug("tx packet received from " + str(self.host) + ":" + str(self.port))

        self._dump_msg('tx', timestamp, tx)


    def _on_block_received(self, connection, block):
        timestamp = str(time.time())
        logging.debug("block packet received from " + str(self.host) + ":" + str(self.port))

        self._dump_msg('block', timestamp, block)


    def _on_addr_received(self, connection, addr):
        timestamp = str(time.time())
        logging.debug("addr packet received from " + str(self.host) + ":" + str(self.port))

        self._dump_msg('addr', timestamp, addr)


    def _on_verack_received(self, connection, verack):
        timestamp = str(time.time())

        logging.debug("verack packet received from " + str(self.host) + ":" + str(self.port))

        self._dump_msg('verack', timestamp, verack)


    def _on_getaddr_received(self, connection, getaddr):
        timestamp = str(time.time())
        #is this msg triggered even though its no explicitely defined in bitcoin.messages?
        logging.debug("getaddr packet received from " + str(self.host) + ":" + str(self.port))

        self._dump_msg('getaddr', timestamp, getaddr)


    def _dump_msg(self, type, timestamp, packet):
        """
        serializes msg into the slog file (not humanly readable)
        :param type:
        :param timestamp:
        :param packet:
        """
        if self.log_file:
            self.log_file.write(TYPE_FLAG + type + "\n")
            self.log_file.write(TIME_FLAG + timestamp + "\n")
            self.log_file.write(SERIALIZATION_FLAG + "\n" + dumps(packet, -1) + "\n")
            self.log_file.write(SECTION_END_FLAG + "\n")
            self.log_file.flush()

def load_all_slog_files(path):
    """
    :return: [{host, port, packet, type_code, timestamp}]
    """
    if isdir(path):
        # return [load_slog_file(join(path, f)) for f in listdir(path) if f.endswith(LOG_FILE_EXT)]
        res = []
        for f in listdir(path):
            if f.endswith(V_LOG_FILE_EXT):
                host, port = extract_data_from_name(f)
                for d in load_slog_file(join(path, f)):
                    d['host'] = host
                    d['port'] = port
                    res.append(d)
        return res
    else:
        raise IOError

def extract_data_from_name(file_name):
    data1 = re.split('_', file_name) #TODO maybe use more sophisticated version of extracting data
    data2 = data1[1].split(':')
    data3 = data2[1].split('.')
    host = data2[0]
    port = data3[0]
    return host, port

def load_slog_file(path):
    if path.endswith(V_LOG_FILE_EXT):
        log_file = open(path, 'r')
        packets = []

        while True:
            packet_data = _load_slog_section(log_file)
            if packet_data is None:
                break
            else:
                packets.append(packet_data)

        logging.debug("load_slog_file result length: " + str(len(packets)))
        log_file.close()
        return packets
    else:
        raise IOError



def _load_slog_section(filepointer):
    line = ""
    content = ""

    while True:
        line = filepointer.readline()
        if not line:
            return None
        elif line.startswith(TYPE_FLAG):
            break

    type_code = line.split(':')[1].strip('\n')
    type = parsers[type_code]()
    line = filepointer.readline()
    timestamp = line.split(':')[1].strip('\n')
    filepointer.readline()  #skip SERIAL_FLAG

    while True:
        line = filepointer.readline()
        if not line or line.startswith(SECTION_END_FLAG):
            break
        content = content + line

    #logging.debug("content: " + str(content))
    result = loads(content)

    #logging.debug("_load_slog_section result: " + str(result))
    return {'packet': result, 'type': type_code, 'timestamp': timestamp}



# _type_code = {
#     'version': VersionPacket,
#     'inv': InvPacket,
#     'ping': PingPacket,
#     'tx': TxPacket,
#     'block': BlockPacket,
#     'addr': AddrPacket,
#     'verack': VerackMessage,
#     'getdata':
# }