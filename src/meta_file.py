__author__ = 'mlei'

import logging
from config import *
from os.path import join
from pickle import dumps, loads
import json

def write_meta_file(path, tx_a, tx_v, inv_a, inv_v, tx_a_send_time, tx_v_send_time, inv_send_time):
    conf = open(CONFIG_PATH, 'r')
    f = open(join(path, META_DATA_NAME + META_DATA_EXT), 'w')

    for line in conf:
        f.write(line)

    f.write('\n')
    f.write('TX_A\n')
    f.write(dumps(tx_a, -1) + '\n')
    f.write('TX_V\n')
    f.write(dumps(tx_v, -1) + '\n')
    f.write('INV_A\n')
    f.write(dumps(inv_a, -1) + '\n')
    f.write('INV_V\n')
    f.write(dumps(inv_v, -1) + '\n')
    f.write('TX_A_SEND_TIME\n')
    f.write(dumps(tx_a_send_time, -1) + '\n')
    f.write('TX_V_SEND_TIME\n')
    f.write(dumps(tx_v_send_time, -1) + '\n')
    f.write('INV_SEND_TIME\n')
    f.write(dumps(inv_send_time, -1) + '\n')

    f.flush()
    f.close()


def load_meta_file(path):
    if path.endswith(META_DATA_NAME+META_DATA_EXT):
        f = open(path, 'r')

        while True:
            line = f.readline()
            if not line:
                raise IOError("metafile format not correct")
            elif line.startswith('TX_A'):
                break

        payload = ''
        while True:
            line = f.readline()
            if not line:
                raise IOError("metafile format not correct")
            elif line.startswith('TX_V'):
                break
            else:
                payload = payload + line

        tx_a = loads(payload.strip('\n'))

        payload = ''
        while True:
            line = f.readline()
            if not line:
                raise IOError("metafile format not correct")
            elif line.startswith('INV_A'):
                break
            else:
                payload = payload + line

        tx_v = loads(payload.strip('\n'))

        payload = ''
        while True:
            line = f.readline()
            if not line:
                raise IOError("metafile format not correct")
            elif line.startswith('INV_V'):
                break
            else:
                payload = payload + line

        inv_a = loads(payload.strip('\n'))

        payload = ''
        while True:
            line = f.readline()
            if not line:
                raise IOError("metafile format not correct")
            elif line.startswith('TX_A_SEND_TIME'):
                break
            else:
                payload = payload + line

        inv_v = loads(payload.strip('\n'))

        payload = ''
        while True:
            line = f.readline()
            if not line:
                raise IOError("metafile format not correct")
            elif line.startswith('TX_V_SEND_TIME'):
                break
            else:
                payload = payload + line

        tx_a_send_time = loads(payload.strip('\n'))

        payload = ''
        while True:
            line = f.readline()
            if not line:
                raise IOError("metafile format not correct")
            elif line.startswith('INV_SEND_TIME'):
                break
            else:
                payload = payload + line

        tx_v_send_time = loads(payload.strip('\n'))

        payload = ''
        while True:
            line = f.readline()
            if not line:
                break
            else:
                payload = payload + line

        inv_send_time = loads(payload.strip('\n'))

        return {'tx_a': tx_a, 'tx_v': tx_v, 'inv_a': inv_a, 'inv_v': inv_v, 'tx_a_send_time': tx_a_send_time, 'tx_v_send_time': tx_v_send_time, 'inv_send_time': inv_send_time}
    else:
        raise IOError("File not found")

def write_victim_meta_file(path, victim):
    """
    writes all true peers of the victim into a file (including those which it could not connect to)
    """
    f = open(join(path, VICTIM_DATA_FILE_NAME + META_DATA_EXT), 'w')

    for peer in victim.true_peer_connections:
        host = peer[0]
        port = peer[1]
        f.write(str(host) + ":" + str(port) + "\n")

    f.flush()
    f.close()

def load_victim_meta_file(path):
    if path.endswith(VICTIM_DATA_FILE_NAME + META_DATA_EXT):
        f = open(path, 'r')
        res = []

        for line in f:
            if line:
                data = line.strip('\n').split(':')
                host = data[0]
                port = data[1]
                res.append((host, port))

        f.close()
        return res
    else:
        raise IOError("File not found")


def write_attacker_log(path, timestamp, log):
    f = open(join(path, str(timestamp) + A_LOG_FILE_EXT), 'w')

    f.write(dumps(log, -1))
    f.flush()
    f.close()


def load_attacker_log(path):
    """
    :return: list of dictionaries
    """
    if path.endswith(A_LOG_FILE_EXT):
        f = open(path, 'r')

        content = f.read()
        f.close()
        return loads(content)
    else:
        raise IOError("File not found")


def write_attacker_meta(path, attacker):
    f = open(join(path, ATTACKER_META_FILE_NAME + META_DATA_EXT), 'w')

    f.write("LOCAL_PEERS\n")
    for peer in attacker.local_peers:
        f.write(str(peer[0]) + ":" + str(peer[1]) + "\n")

    f.write("OTHER_PEERS\n")
    for peer in attacker.other_peers:
        f.write(str(peer[0]) + ":" + str(peer[1]) + "\n")

def load_attacker_meta(path):
    if path.endswith(ATTACKER_META_FILE_NAME + META_DATA_EXT):
        f = open(path, 'r')
        local_peers = []
        other_peers = []

        while True:
            line = f.readline()
            if not line:
                raise IOError("Metafile format not correct")
            elif line.startswith("LOCAL_PEERS"):
                break

        while True:
            line = f.readline()
            if not line:
                raise IOError("Metafile format not correct")
            elif line.startswith("OTHER_PEERS"):
                break
            else:
                data = line.strip('\n').split(':')
                local_peers.append((data[0], data[1]))

        while True:
            line = f.readline()
            if not line:
                break
            else:
                data = line.strip('\n').split(':')
                other_peers.append((data[0], data[1]))

        f.close()
        return local_peers, other_peers
    else:
        raise IOError("File not found")

def load_result_file(path):
    tx_as = []
    tx_vs = []
    timeframe = None
    data = None
    if path.endswith(RESULT_FILE_NAME + META_DATA_EXT):
        f = open(path, 'r')
        while True:
            line = f.readline()
            if not line:
                raise IOError("Metafile format no correct")
            elif line.startswith("Tx_v received"):
                break
        data = f.readline()
        tx_vs = json.loads(data)

        while True:
            line = f.readline()
            if not line:
                raise IOError("Metafile format no correct")
            elif line.startswith("Tx_a received"):
                break
        data = f.readline()
        tx_as = json.loads(data)

        while True:
            line = f.readline()
            if not line:
                return tx_as, tx_vs, None
            elif line.startswith("Transaction timeframe"):
                break
        data = line.split(' ')[-1]
        timeframe = float(data.strip('\n'))
        return tx_as, tx_vs, timeframe
    else:
        raise IOError("File not found")