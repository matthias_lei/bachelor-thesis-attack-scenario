from src.config import N_GETADDR, REQUEST_INTERVAL,  TEST_MODE, TEST_NET_DNS_LIST, MAIN_NET_DNS_ADDRESS

__author__ = 'mlei'

from subprocess import call, check_output, PIPE, Popen, STDOUT
from bitcoin import network, messages
from gevent import spawn_later, sleep
import config
import logging
import urllib2
import json
from IPy import IP
from gevent.lock import Semaphore


def tx_packet_to_string(tx):
    inputs = tx.inputs
    outputs = tx.outputs
    lock_time = tx.lock_time
    inputs_hex = [((txid.encode('hex'), t), script.encode('hex'), seq) for ((txid, t), script, seq) in inputs]
    outputs_hex = [(amount, addr) for (amount, addr) in outputs]

    res = "{\n\tinputs: " + str(inputs_hex) + "\n\toutputs: " + str(outputs_hex) + "\n\tlock_time: " + str(lock_time) + "\n}"
    return res


def get_local_client_version():
    version = Popen(["bitcoin-qt", "-version"], stdout=PIPE).stdout
    return version.readline().strip('\n')

def getLocalPeers():
    """
    :return: returns a list of ip-addresses which the local bitcoin core is connected to
    """
    res = []
    netstat = Popen(['netstat', '-anp'], stdout=PIPE, stderr=STDOUT)
    grep1 = Popen(['grep', 'bitcoin'], stdin=netstat.stdout, stdout=PIPE)
    grep2 = Popen(['grep', 'ESTABLISHED'], stdin=grep1.stdout, stdout=PIPE)
    awk = Popen(['awk', '{print $5}'], stdin=grep2.stdout, stdout=PIPE)

    for line in awk.stdout:
        ip = str(line).split(":").pop(0)
        if not ip == '127.0.0.1':
            res.append(ip)

#   for line in awk.stdout:
#        print str(line).split(":").pop(0)

    return res

def get_random_addresses():
    """
    on MAIN_NET: fetches addresses over JSON at node.info
    on TEST_NET: ddig at hardcoded TEST_NET addresses of the official Bitcoin Core source (chainparaams.cpp)
    :return:(host, port)
    """
    res = []
    if not TEST_MODE:
        u = json.loads(urllib2.urlopen(MAIN_NET_DNS_ADDRESS).read())['results'][0]['url']
        print str(u)
        if len(u) == 0:
            return None

        nodes = json.loads(urllib2.urlopen(u).read())['nodes']

        for addr in nodes.keys():
            tokens = addr.split(':')
            try:
                host = tokens[0]
                IP(host)            #check whether hoststring is valid ip
                port = int(tokens[1])
                res.append((host, port))
            except ValueError:
                pass
    else:   #TEST_MODE
        for dns in TEST_NET_DNS_LIST:
            dig = Popen(['dig', dns], stdout=PIPE)
            for addr in _parse_dig_answer(dig.stdout):
                res.append((addr, 18883))

    logging.debug("number of random addresses received: " + str(len(res)))

    return res


def tx_equals(tx1, tx2):
    return tx1.hash() == tx2.hash()


def _parse_dig_answer(output):
    critical_section = False
    res = []
    for line in output:
        if critical_section:                            #read from critical section
            if not line == "\n":                        #is answer
                res.append(line.split('\t')[-1].strip('\n'))
            else:                                       #end reached
                critical_section = False

        elif line.startswith(";; ANSWER SECTION:"):     #found answer
            critical_section = True

    logging.debug("_parse_dig_answer result: " + str(res))

    return res

class NLock(object):
    """
    Lock that unlocks only if release has been called n times
    """

    def __init__(self, n, timeout=0):
        self.lock = Semaphore(0)
        self._counter_lock = Semaphore(1)
        self.counter = n
        self.timeout = timeout

    def acquire(self):
        sleep(0)
        if self.timeout > 0:
            spawn_later(self.timeout, self.lock.release)
        self.lock.acquire()

    def release(self):
        self._counter_lock.acquire()
        self.counter -= 1
        if self.counter == 0:
            self.lock.release()
        self._counter_lock.release()

    def locked(self):
        self._counter_lock.acquire()
        res = self.lock.locked()
        self.release()
        return res