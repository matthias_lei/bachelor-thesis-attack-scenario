__author__ = 'mlei'

import logging
import config
from utils import *
from node_logger import *
import matplotlib.pyplot as plt
import numpy as np
from meta_file import load_victim_meta_file, load_attacker_log, load_attacker_meta
from bitcoin.network import *

class plotter(object):

    def __init__(self, timestamp, path, tx_a, tx_v, inv_a, inv_v, tx_v_send_time, tx_a_send_time, inv_send_time):
        self.timestamp = timestamp
        self.path = path
        logging.info("analyser_path: " + str(self.path))
        self.tx_a = tx_a
        self.tx_v = tx_v
        self.inv_a = inv_a
        self.inv_v = inv_v

        self.tx_v_send_time = tx_v_send_time
        self.tx_a_send_time = tx_a_send_time
        self.inv_send_time = inv_send_time

        #victim data needs to be filtered by isrelevant
        self.victim_true_peers = load_victim_meta_file(join(self.path, VICTIM_DATA_FILE_NAME + META_DATA_EXT))
        self.attacker_local_peers, self.attacker_other_peers = load_attacker_meta(join(self.path, ATTACKER_META_FILE_NAME + META_DATA_EXT))
        self.attacker_data = load_attacker_log(join(self.path, str(timestamp) + A_LOG_FILE_EXT))
        logging.debug("attacker_data length: " + str(len(self.attacker_data)))

        self.victim_data = self.double_spend_history()
        logging.debug("victim data:\n" + str(self.victim_data))
        self.draw_timeline()


    def double_spend_history(self):
        res = []

        for e in load_all_slog_files(self.path):
            if self._is_relevant(e):
                res.append(e)

        return self._sort_data(res)

    def draw_timeline(self):
        fig, axs = plt.subplots(2, 1, sharex=True)
        LENGTH, WIDTH = 18.5, 15

        ###### draw true peers

        peer_data = self._format_true_peer_data()
        ip_map = self.number_map(self.victim_true_peers)
        y_ticks = sorted([p[0] for p in self.victim_true_peers], lambda x, y: cmp(ip_map[p[0]], ip_map[p[0]]))

        verack_data = zip(*[(i[0], ip_map[i[1]]) for i in peer_data[0]])
        inv_data = zip(*[(i[0], ip_map[i[1]]) for i in peer_data[1]])
        tx_v_data = zip(*[(i[0], ip_map[i[1]]) for i in peer_data[2]])
        tx_a_data = zip(*[(i[0], ip_map[i[1]]) for i in peer_data[3]])

        logging.debug("victim verack_data length: " + str(len(peer_data[0])))
        logging.debug("victim inv_data length: " + str(len(peer_data[1])))
        logging.debug("victim tx_a_data length: " + str(len(peer_data[2])))
        logging.debug("victim tx_v_data length: " + str(len(peer_data[3])))

        if len(verack_data) == 0:
            verack_data = ([], [])
        if len(inv_data) == 0:
            inv_data = ([], [])
        if len(tx_v_data) == 0:
            tx_v_data = ([], [])
        if len(tx_a_data) == 0:
            tx_a_data = ([], [])

        axs[0].plot(verack_data[0], verack_data[1], 'ro', color='blue', label='Connection established')
        axs[0].plot(inv_data[0], inv_data[1], 'ro', color='yellow', label='Inv packet')
        axs[0].plot(tx_v_data[0], tx_v_data[1], 'ro', color='green', label='Tx_v packet')
        axs[0].plot(tx_a_data[0], tx_a_data[1], 'ro', color='red', label='Tx_a packet')

        txs = peer_data[2] + peer_data[3]
        x_max = round(max(txs, key=lambda x: x[0])[0] * 1.2) if txs else 90
        axs[0].set_xlabel("seconds")
        axs[0].set_ylabel("True peers")
        axs[0].set_xlim(left=0, right=x_max)
        axs[0].set_xticks(np.arange(0, x_max, 5))
        axs[0].set_ylim([-0.5, len(ip_map)-0.5])
        axs[0].set_yticks(np.arange(len(ip_map)))
        axs[0].set_yticklabels(y_ticks)

        print str(self.tx_v_send_time-self.timestamp) + " " + str(self.tx_a_send_time-self.timestamp)
        axs[0].plot([self.tx_v_send_time-self.timestamp, self.tx_v_send_time-self.timestamp], axs[0].get_ylim(), 'k-', color='green', label="Tx_v send time")
        axs[0].plot([self.tx_a_send_time-self.timestamp, self.tx_a_send_time-self.timestamp], axs[0].get_ylim(), 'k-', color='red', label="Tx_a send time")
        axs[0].plot([self.inv_send_time-self.timestamp, self.inv_send_time-self.timestamp], axs[0].get_ylim(), 'k-', color='yellow', label="Inv send time")

        axs[0].legend(bbox_to_anchor=(0., 1.02, 1., .102), loc=3,
           ncol=2, mode="expand", borderaxespad=0.)
        axs[0].grid(True)


        ###### draw local peers

        verack_data, getdata_data, tx_v_data = self._format_local_peer_data()
        ip_map = self.number_map(self.attacker_local_peers)
        y_ticks = sorted([p[0] for p in self.attacker_local_peers], lambda x, y: cmp(ip_map[p[0]], ip_map[p[0]]))

        logging.debug("attacker local verack_data length: " + str(len(verack_data)))
        logging.debug("attacker local getdata_data length: " + str(len(getdata_data)))
        logging.debug("attacker local tx_data length: " + str(len(tx_v_data)))

        verack_data = zip(*[(i[0], ip_map[i[1]]) for i in verack_data])
        getdata_data = zip(*[(i[0], ip_map[i[1]]) for i in getdata_data])
        tx_v_data = zip(*[(i[0], ip_map[i[1]]) for i in tx_v_data])

        if len(verack_data) == 0:
            verack_data = ([], [])
        if len(getdata_data) == 0:
            getdata_data = ([], [])
        if len(tx_v_data) == 0:
            tx_v_data = ([], [])

        axs[1].plot(verack_data[0], verack_data[1], 'ro', color='blue', label='Connection established')
        axs[1].plot(getdata_data[0], getdata_data[1], 'ro', color='purple', label='Getdata packet')
        #axs[1].plot(tx_v_data[0], tx_v_data[1], 'ro', color='green', label='Tx_v packet')

        axs[1].set_xlabel("seconds")
        axs[1].set_ylabel("Local peers")
        axs[1].set_ylim([-0.5, len(ip_map)-0.5])
        axs[1].set_yticks(np.arange(len(ip_map)))
        axs[1].set_yticklabels(y_ticks)

        axs[1].plot([self.tx_v_send_time-self.timestamp, self.tx_v_send_time-self.timestamp], axs[1].get_ylim(), 'k-', color='green', label="Tx_v send time")
        axs[1].plot([self.tx_a_send_time-self.timestamp, self.tx_a_send_time-self.timestamp], axs[1].get_ylim(), 'k-', color='red', label="Tx_a send time")
        axs[1].plot([self.inv_send_time-self.timestamp, self.inv_send_time-self.timestamp], axs[1].get_ylim(), 'k-', color='yellow', label="Inv send time")

        axs[1].legend(bbox_to_anchor=(0., 1.02, 1., .102), loc=3,
                      ncol=2, mode="expand", borderaxespad=0.)
        axs[1].grid(True)


        fig.set_size_inches(LENGTH, WIDTH)
        #fig.tight_layout()
        plt.subplots_adjust(hspace=0.3)
        fig.savefig(join(self.path, "true_and_local_peers_history.png"))

        ####### draw other peers

        fig, ax = plt.subplots(1, 1)

        ip_map = self.number_map(self.attacker_other_peers)
        y_ticks = sorted([p[0] for p in self.attacker_other_peers], lambda x, y: cmp(ip_map[p[0]], ip_map[p[0]]))


        verack_data, getdata_data, tx_a_data = self._format_other_peer_data()

        verack_data = zip(*[(i[0], ip_map[i[1]]) for i in verack_data])
        getdata_data = zip(*[(i[0], ip_map[i[1]]) for i in getdata_data])
        tx_a_data = zip(*[(i[0], ip_map[i[1]]) for i in tx_a_data])

        if len(verack_data) == 0:
            verack_data = ([], [])
        if len(getdata_data) == 0:
            getdata_data = ([], [])
        if len(tx_a_data) == 0:
            tx_a_data = ([], [])

        ax.plot(verack_data[0], verack_data[1], 'ro', color='blue', label='Connection established')
        ax.plot(getdata_data[0], getdata_data[1], 'ro', color='purple', label='Getdata packet')
        #ax.plot(tx_a_data[0], tx_a_data[1], 'ro', color='green', label='Tx_a packet')

        ax.set_xlabel("seconds")
        ax.set_ylabel("Other peers")
        ax.set_ylim([-0.5, len(ip_map)-0.5])
        ax.set_yticks(np.arange(len(ip_map)))
        ax.set_yticklabels([])
        #ax.set_yticklabels(y_ticks)
        ax.tick_params(axis='y', labelsize=6)
        ax.set_xlim(left=0, right=x_max)
        ax.set_xticks(np.arange(0, x_max, 5))
        ax.legend(bbox_to_anchor=(0., 1.02, 1., .102), loc=3,
                  ncol=2, mode="expand", borderaxespad=0.)
        ax.grid(True)

        fig.set_size_inches(LENGTH, WIDTH)
        fig.savefig(join(self.path, "other_peers_history.png"))
        #plt.show()


    def _format_true_peer_data(self):
        veracks = [(float(d['timestamp'])-self.timestamp, d['host']) for d in self.victim_data if d['type'] == 'verack']
        invs = [(float(d['timestamp'])-self.timestamp, d['host']) for d in self.victim_data if d['type'] == 'inv']
        tx_vs = [(float(d['timestamp'])-self.timestamp, d['host']) for d in self.victim_data if d['type'] == 'tx' and tx_equals(d['packet'], self.tx_v)]
        tx_as = [(float(d['timestamp'])-self.timestamp, d['host']) for d in self.victim_data if d['type'] == 'tx' and tx_equals(d['packet'], self.tx_a)]

        return veracks, invs, tx_vs, tx_as

    def _format_local_peer_data(self):
        veracks = [(float(d['timestamp'])-self.timestamp, d['host']) for d in self.attacker_data if d['type'] == ConnectionEstablishedEvent.type and d['target'] == 'local']
        getdata = [(float(d['timestamp'])-self.timestamp, d['host']) for d in self.attacker_data if d['type'] == 'getdata' and d['target'] == 'local']
        tx_vs = [(float(d['timestamp'])-self.timestamp, d['host']) for d in self.attacker_data if d['type'] == 'tx' and d['target'] == 'local']

        return veracks, getdata, tx_vs

    def _format_other_peer_data(self):
        veracks = [(float(d['timestamp'])-self.timestamp, d['host']) for d in self.attacker_data if d['type'] == ConnectionEstablishedEvent.type and d['target'] == 'other']
        getdata = [(float(d['timestamp'])-self.timestamp, d['host']) for d in self.attacker_data if d['type'] == 'getdata' and d['target'] == 'other']
        tx_as = [(float(d['timestamp'])-self.timestamp, d['host']) for d in self.attacker_data if d['type'] == 'tx' and d['target'] == 'other']

        return veracks, getdata, tx_as


    def _sort_data(self, data):
        return sorted(data, (lambda x, y: cmp(x['timestamp'], y['timestamp'])))


    def _is_relevant(self, data):
        type = data['type']
        packet = data['packet']
        host = data['host']
        if host == '127.0.0.1':
            return False
        elif type == 'tx' and (tx_equals(packet, self.tx_a) or tx_equals(packet, self.tx_v)):
            return True
        elif type == 'inv' and ((self.inv_a.hashes[0] in packet.hashes) or (self.inv_v.hashes[0] in packet.hashes)):
            return True
        elif type == 'verack':
            return True
        else:
            return False

    def number_map(self, data):
        map = {}
        counter = 0
        for ip in [i[0] for i in data]:
            if ip not in map.keys():
                map[ip] = counter
                counter += 1
        return map