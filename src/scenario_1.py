__author__ = 'mlei'

from config import *
import logging
from utils import *
import random
import config
from time import time
from attacker import attacker
from victim import victim
from os.path import join, exists, isfile, isdir
import os
from plotter import plotter
from meta_file import *
from evaluator import *
from btc_client import restart_client, get_unspent_output, client_is_running

class scenario_1(object):

    def __init__(self, tx_delay=DEFAULT_TX_DELAY):

        self.tx_delay = tx_delay

        if not client_is_running():
            restart_client()

        #wait until main output can be used (may take a while, approx 5min -15min)
        while True:
            self.unspent_outs = [d for d in sorted(get_unspent_output(), lambda x, y: -cmp(x['amount'], y['amount'])) if d['confirmations'] > 0]
            if len(self.unspent_outs) > 0:
                self.unspent_out = self.unspent_outs[0]
                break
            sleep(CONFIRMATION_RETRY_TIME)

        self.timestamp = time()
        self.path = join(SCENARIOS_FOLDER_NAME, SCENARIO_PREFIX + str(self.timestamp) + " " + str(tx_delay))
        self.address_pool = get_random_addresses()
        random.shuffle(self.address_pool)

        logging.getLogger().setLevel(logging.DEBUG)
        # logging.basicConfig(filename=join(self.path, 'console.log'), level=logging.DEBUG)

        if not exists(SCENARIOS_FOLDER_NAME):
            os.mkdir(SCENARIOS_FOLDER_NAME)
        if not exists(self.path):
            os.mkdir(self.path)

        # logger = logging.getLogger()
        # hdlr = logging.FileHandler(join(self.path, "stdlog"))
        # ch = logging.StreamHandler()
        # hdlr.setLevel(logging.DEBUG)
        # ch.setLevel(logging.DEBUG)
        #
        #
        # formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
        # hdlr.setFormatter(formatter)
        # logger.addHandler(hdlr)
        # logger.addHandler(ch)
        # logger.setLevel(logging.DEBUG)


        logging.info(str(len(self.address_pool)) + " random addresses fetched")

        #create pool of connection peers (some might not connect)
        #create victim connections,
        self.true_peers_pool = [self.address_pool.pop() for _ in range(0, 4*N_TRUE_LOCAL_PEERS)]

        if not exists(SCENARIOS_FOLDER_NAME):
            os.mkdir(SCENARIOS_FOLDER_NAME)
        if not exists(self.path):
            os.mkdir(self.path)

        self.victim = victim(self.path, self.timestamp, self.true_peers_pool, (HOST, PORT))
        logging.info("victim created")

        self.victim.connect_to_all_peers()
        logging.info("victim connected to all peers")

        #establish attacker connections

        self.true_peers = self.victim.true_peer_connections

        self.local_peers = [self.address_pool.pop() for _ in range(0, N_FALSE_POSITIVE_PEERS)] + self.victim.true_peer_connections

        self.other_peers = []

        for _ in range(0, min(len(self.address_pool), N_OTHER_PEERS)):
            self.other_peers.append(self.address_pool.pop())

        logging.info(str(len(self.true_peers)) + " true peers")
        logging.info(str(len(self.local_peers)) + " local peers")
        logging.info(str(len(self.other_peers)) + " other peers")

        self.attacker = attacker(self.path, self.timestamp, self.local_peers, self.other_peers)
        logging.info("attacker created")

        self.attacker.connect_to_all_peers()
        logging.info("attacker connected to all peers")

        logging.info("initialising bait")
        self.inv_send_time = time()
        self.attacker.prime_peers()


        logging.info("sending transactions")
        self.tx_v_send_time = time()
        logging.error("tx send start: " + str(self.tx_v_send_time))
        self.tx_a_send_time = self.tx_v_send_time + self.tx_delay
        self.attacker.send_txs(self.tx_delay)
        tmp = time()
        logging.error("tx send end: " + str(tmp))
        logging.error("duration: " + str(tmp-self.tx_v_send_time))

        logging.info("listening and logging for double spends")
        sleep(LISTENING_AFTER_TX_SENT)

        self.victim.stop()
        self.attacker.stop()

        logging.info("tx_a hash: " + str(self.attacker.tx_a.hash()).encode('hex'))
        logging.info("tx_v hash: " + str(self.attacker.tx_v.hash()).encode('hex'))

        write_meta_file(self.path,
                        self.attacker.tx_a,
                        self.attacker.tx_v,
                        self.attacker.inv_a,
                        self.attacker.inv_v,
                        self.tx_a_send_time,
                        self.tx_v_send_time,
                        self.inv_send_time)

        self.analyser = plotter(self.timestamp,
                                self.path,
                                self.attacker.tx_a,
                                self.attacker.tx_v,
                                self.attacker.inv_a,
                                self.attacker.inv_v,
                                self.tx_v_send_time,
                                self.tx_a_send_time,
                                self.inv_send_time)

        self.eval = evaluator(self.timestamp,
                              self.path,
                              self.attacker.tx_a,
                              self.attacker.tx_v,
                              self.attacker.inv_a,
                              self.attacker.inv_v)

        self.end_time = time()

        pass
